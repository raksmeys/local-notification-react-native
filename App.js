import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import {showNotification, scheduleNotification, getChannel} from './src/notification.server'
import OneSignal from 'react-native-onesignal'

export default class App extends Component {

  componentDidMount(){
    console.log("object")
    OneSignal.init('8a6ad892-87d1-446f-8cbf-b82be00d0fbf')
    OneSignal.addEventListener('received', (data) => {
      console.log(data);
    });

  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received');
  }



  render() {
    return (
      <View style={styles.container}>
        <Text> Welcome </Text>
        <TouchableOpacity
          onPress={() => showNotification('Hello', 'React native')}
          style={styles.btn}
        >
          <Text>Show Me</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => scheduleNotification('Hello', 'React native after 5sec')}
          style={styles.btn}
        >
          <Text>Show Me after 5 sec</Text>
        </TouchableOpacity>
        
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    backgroundColor: 'lightblue',
    width: 200,
    height: 70,
    borderRadius: 35,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
